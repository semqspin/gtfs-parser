# gtfs-parser

unzip data files in each of following directories

```
data/delijn
data/sncb
data/stib
data/tec
```

install mongodb and make sure `mongod` is running

from root directory, run:

```
npm install
node src
```

the process takes several minutes, depending on data size.

Program is using streams in order to allow large gtfs file to be processed while keeping memory usage low.

on core i5-4670 (3.4 GHz) / 8 GB RAM / SSD

-   memory usage of node process under 100 MB
-   importation time 00:23:30
