module.exports = {
    uri: 'mongodb://127.0.0.1/dev-gtfs',
    options: {
        useNewUrlParser: true,
    },
};
