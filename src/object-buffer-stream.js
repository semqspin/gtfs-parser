const stream = require('stream');

class ObjectBufferStream extends stream.Transform {
    constructor(bufferMaxLength) {
        super({ objectMode: true });
        this.maxLength = bufferMaxLength;
        this.buffer = [];
    }

    _transform(chunk, encoding, callback) {
        this.buffer.push(chunk);
        if (this.buffer.length >= this.maxLength) {
            this.push(this.buffer);
            this.buffer = [];
        }
        callback();
    }

    _flush(callback) {
        if (this.buffer.length) {
            this.push(this.buffer);
            this.buffer = [];
        }
        callback();
    }
}

module.exports = ObjectBufferStream;
