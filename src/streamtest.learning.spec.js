const StreamTest = require('streamtest');

describe('StreamTest', () => {
    test('should have two versions', () => {
        expect(StreamTest.versions).toEqual(['v1', 'v2']);
    });

    test('should create from chunks', done => {
        StreamTest.v2.fromChunks(['hello', 'world']).pipe(
            StreamTest.v2.toText((err, text) => {
                if (err) {
                    done(err);
                }
                expect(text).toBe('helloworld');
                done();
            }),
        );
    });

    test('should preserve chunk (not joining or splitting)', done => {
        StreamTest.v2.fromChunks(['one', 'two', 'three']).pipe(
            StreamTest.v2.toChunks((err, chunks) => {
                if (err) {
                    done(err);
                }
                expect(chunks.map(c => c.toString())).toEqual([
                    'one',
                    'two',
                    'three',
                ]);
                done();
            }),
        );
    });
});
