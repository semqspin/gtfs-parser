const fs = require('fs');
const _ = require('lodash');
const pathJoin = require('path').join;
const dal = require('./dal')(require('./dal/config'));
const LineStream = require('./line-stream');
const GtfsStream = require('./gtfs-stream');
const ObjectBufferStream = require('./object-buffer-stream');
const LimitedParallelStream = require('./limited-parallel-stream');
const async = require('async');

const data = [
    { path: 'delijn/agency.txt', model: dal.Agency },
    { path: 'delijn/calendar_dates.txt', model: dal.CalendarDate },
    { path: 'delijn/routes.txt', model: dal.Route },
    { path: 'delijn/stop_times.txt', model: dal.StopTime },
    { path: 'delijn/stops.txt', model: dal.Stop },
    { path: 'delijn/trips.txt', model: dal.Trip },

    { path: 'sncb/agency.txt', model: dal.Agency },
    { path: 'sncb/calendar.txt', model: dal.Calendar },
    { path: 'sncb/calendar_dates.txt', model: dal.CalendarDate },
    { path: 'sncb/routes.txt', model: dal.Route },
    { path: 'sncb/stop_times.txt', model: dal.StopTime },
    { path: 'sncb/stops.txt', model: dal.Stop },
    { path: 'sncb/transfers.txt', model: dal.Transfer },
    { path: 'sncb/translations.txt', model: dal.Translation },
    { path: 'sncb/trips.txt', model: dal.Trip },

    { path: 'stib/agency.txt', model: dal.Agency },
    { path: 'stib/calendar.txt', model: dal.Calendar },
    { path: 'stib/calendar_dates.txt', model: dal.CalendarDate },
    { path: 'stib/routes.txt', model: dal.Route },
    { path: 'stib/shapes.txt', model: dal.Shape },
    { path: 'stib/stop_times.txt', model: dal.StopTime },
    { path: 'stib/stops.txt', model: dal.Stop },
    { path: 'stib/translations.txt', model: dal.Translation },
    { path: 'stib/trips.txt', model: dal.Trip },

    { path: 'tec/agency.txt', model: dal.Agency },
    { path: 'tec/calendar.txt', model: dal.Calendar },
    { path: 'tec/calendar_dates.txt', model: dal.CalendarDate },
    { path: 'tec/routes.txt', model: dal.Route },
    { path: 'tec/shapes.txt', model: dal.Shape },
    { path: 'tec/stop_times.txt', model: dal.StopTime },
    { path: 'tec/stops.txt', model: dal.Stop },
    { path: 'tec/trips.txt', model: dal.Trip },
];
console.time();
main(data)
    .then(() => {
        console.log('successfully completed');
        console.timeEnd();
        process.exit(0);
    })
    .catch(err => {
        console.log(err);
        console.timeEnd();
        process.exit(-1);
    });

async function main(data) {
    const models = _(data)
        .map('model')
        .uniq()
        .value();
    await clearCollections(models);

    console.time('import');
    const concurrencyLimit = 20;
    await new Promise((resolve, reject) => {
        async.mapLimit(data, concurrencyLimit, importCollection, err => {
            if (err) {
                return reject(err);
            }
            resolve();
        });
    });
    console.timeEnd('import');

    const counts = await Promise.all(
        models.map(model => model.countDocuments()),
    );
    counts.forEach((count, index) =>
        console.log(models[index].modelName, count),
    );
}

async function importCollection({ path, model }) {
    await new Promise((resolve, reject) => {
        const fullPath = pathJoin(__dirname, '../data', path);
        console.log('handling', fullPath);

        fs.createReadStream(fullPath, { encoding: 'utf8' })
            .pipe(new LineStream())
            .pipe(new GtfsStream())
            .pipe(new ObjectBufferStream(64))
            .pipe(
                new LimitedParallelStream(1, async (data, enc, done) => {
                    if (!data) {
                        return done();
                    }
                    await model.insertMany(data);
                    done();
                }),
            )
            .on('error', reject)
            .on('finish', () => {
                console.log(`finished: ${path}`);
                resolve();
            });
    });
}

async function clearCollections(models) {
    console.time('clear');
    console.log('clearing', _.map(models, 'modelName'));
    await Promise.all(models.map(model => model.deleteMany()));
    console.timeEnd('clear');
}
