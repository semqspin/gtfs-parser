const ObjectBufferStream = require('./object-buffer-stream');
const StreamTest = require('streamtest');

describe('ObjectBufferStream', () => {
    // helpers

    const expectObjects = (done, ...expectedObjects) => {
        return StreamTest.v2.toObjects((err, actualObjects) => {
            if (err) {
                done(err);
            }

            expect(actualObjects).toEqual(expectedObjects);
            done();
        });
    };

    // tests

    it('should output objects grouped in array', done => {
        const object = {};
        StreamTest.v2
            .fromObjects([object, object, object, object])
            .pipe(new ObjectBufferStream(2))
            .pipe(expectObjects(done, [object, object], [object, object]));
    });

    it('should flush last objects when buffer is not full', done => {
        const object = {};
        StreamTest.v2
            .fromObjects([object, object, object])
            .pipe(new ObjectBufferStream(2))
            .pipe(expectObjects(done, [object, object], [object]));
    });
});
