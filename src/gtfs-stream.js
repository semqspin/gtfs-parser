const stream = require('stream');
const _ = require('lodash');

class GtfsStream extends stream.Transform {
    constructor() {
        super({ objectMode: true });
        this.headers = null;
    }

    _transform(chunk, encoding, callback) {
        const values = GtfsStream.splitLine(chunk.toString());
        if (!this.headers) {
            this.headers = values;
            return callback();
        }
        this.push(_.zipObject(this.headers, values));

        callback();
    }

    _flush(callback) {
        callback();
    }

    static splitLine(line) {
        // console.log('SPLITTING [%s]', line);
        const res = [];
        if (line === '') {
            return [];
        }
        if (line.charAt(0) === ',') {
            res.push('', line.slice(1));
            return ['', ...GtfsStream.splitLine(line.slice(1))];
        }

        let currentIndex = 1;
        const firstCharIsQuote = line.charAt(0) === '"';
        while (currentIndex < line.length) {
            const currentChar = line.charAt(currentIndex);
            if (currentChar === '"') {
                if (!firstCharIsQuote) {
                    throw Error('unwrapped double quote');
                }
                currentIndex++;
                const nextChar =
                    currentIndex < line.length
                        ? line.charAt(currentIndex)
                        : null;
                if (!nextChar || nextChar === ',') {
                    break;
                } else if (nextChar === '"') {
                    currentIndex++;
                    continue;
                } else {
                    throw Error('unescaped double quote');
                }
            } else if (currentChar === ',' && !firstCharIsQuote) {
                break;
            }
            currentIndex++;
        }

        let value = firstCharIsQuote
            ? line.slice(1, currentIndex - 1)
            : line.slice(0, currentIndex - 0);

        value = value.replace(/""/g, '"');

        if (currentIndex === line.length - 1) {
            return [value, ''];
        }

        return [value, ...GtfsStream.splitLine(line.slice(currentIndex + 1))];
    }
}

module.exports = GtfsStream;
