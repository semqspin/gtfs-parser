const stream = require('stream');
const _ = require('lodash');

class LineStream extends stream.Transform {
    constructor() {
        super({ encoding: 'utf8' });
        this.lastTail = '';
    }

    _transform(chunk, encoding, callback) {
        let text = this.lastTail + chunk.toString();
        text = text.replace(/\r\n/g, '\n').replace(/\r/g, '\n');
        let lines = text.split('\n');
        if (text.charAt(text.length - 1) === '\n') {
            this.lastTail = '';
        } else {
            this.lastTail = _.last(lines);
            lines = _.initial(lines);
        }

        lines.forEach(line => this.push(line));
        callback();
    }

    _flush(callback) {
        if (this.lastTail) {
            this.push(this.lastTail);
        }
        callback();
    }
}

module.exports = LineStream;
