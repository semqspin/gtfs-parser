'use strict';
// const expect = require('chai').expect;
const config = {
    // prefer 127.0.0.1 to localhost in uri, otherwise, initial connection is slow (> 1 second)
    uri: 'mongodb://127.0.0.1:27017/test',
    options: {
        useNewUrlParser: true,
        connectTimeoutMS: 1000,
    },
};
let dal = require('./index')(config);

describe('dal', () => {
    afterAll(async () => {
        // without closing connection, test hangs
        dal.db.close();
    });

    beforeEach(async () => {
        await dal.db.dropDatabase();
    });

    describe('save one agency document', () => {
        beforeEach(async () => {
            await new dal.Agency({
                agency_id: 'B',
                agency_name: 'TEC Brabant Wallon',
                agency_url: 'http://www.infotec.be',
                agency_timezone: 'Europe/Brussels',
                agency_lang: 'fr',
                agency_phone: '',
            }).save();
        });

        it('document can be reloaded from database', async () => {
            const [reloaded, ...tail] = await dal.Agency.find();
            expect(tail).toHaveLength(0);
            expect(reloaded).toBeDefined;
            expect(reloaded.agency_name).toBe('TEC Brabant Wallon');
        });
    });
});
