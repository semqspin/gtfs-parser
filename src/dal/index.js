'use strict';

const mongoose = require('mongoose');

module.exports = function(config) {
    const dal = {};

    createModel('Agency', {
        agency_id: String,
        agency_name: String,
        agency_url: String,
        agency_timezone: String,
        agency_language: String,
        agency_phone: String,
    });

    createModel('Route', {
        route_id: String,
        agency_id: String,
        route_short_name: String,
        route_long_name: String,
        route_desc: String,
        route_type: String,
        route_url: String,
    });

    createModel('Trip', {
        route_id: String,
        service_id: String,
        trip_id: String,
        trip_short_name: String,
        direction_id: String,
        block_id: String,
        shape_id: String,
    });

    createModel('Calendar', {
        service_id: String,
        monday: String,
        tuesday: String,
        wednesday: String,
        thursday: String,
        friday: String,
        saturday: String,
        sunday: String,
        start_date: String,
        end_date: String,
    });

    createModel('CalendarDate', {
        service_id: String,
        date: String,
        exception_type: String,
    });

    createModel('Shape', {
        shape_id: String,
        shape_pt_lat: String,
        shape_pt_lon: String,
        shape_pt_sequence: String,
    });

    createModel('Stop', {
        stop_id: String,
        stop_code: String,
        stop_name: String,
        stop_desc: String,
        stop_lat: String,
        stop_lon: String,
        zone_id: String,
        stop_url: String,
        location_type: String,
    });

    createModel('StopTime', {
        trip_id: String,
        arrival_time: String,
        departure_time: String,
        stop_id: String,
        stop_sequence: String,
        pickup_type: String,
        drop_off_type: String,
    });

    createModel('Transfer', {
        from_stop_id: String,
        to_stop_id: String,
        transfer_type: String,
        min_transfer_time: String,
        from_trip_id: String,
        to_trip_id: String,
    });

    createModel('Translation', {
        trans_id: String,
        translation: String,
        lang: String,
    });

    mongoose.connect(
        config.uri,
        config.options,
    );
    dal.db = mongoose.connection.on('error', err => {
        throw Error('connection error: ' + err);
    });

    return dal;

    function createModel(modelName, schemaDescription) {
        const schema = new mongoose.Schema(schemaDescription);
        dal[modelName] = mongoose.model(modelName, schema);
    }
};
