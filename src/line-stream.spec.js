const LineStream = require('./line-stream');
const StreamTest = require('streamtest');

describe('LineStream', () => {
    // helpers

    const expectLines = (done, ...expectedLines) => {
        return StreamTest.v2.toChunks((err, chunks) => {
            if (err) {
                done(err);
            }

            expect(chunks.map(chunk => chunk.toString())).toEqual(
                expectedLines,
            );
            done();
        });
    };

    it('should split chunk with 2 complete lines in 2 chunks', done => {
        StreamTest.v2
            .fromChunks(['line 1\nline 2\n'])
            .pipe(new LineStream())
            .pipe(expectLines(done, 'line 1', 'line 2'));
    });

    it('should split several chunks with 2 complete lines in 2 chunks', done => {
        StreamTest.v2
            .fromChunks(['line 1\nline 2\n', 'line 3\nline 4\n'])
            .pipe(new LineStream())
            .pipe(expectLines(done, 'line 1', 'line 2', 'line 3', 'line 4'));
    });

    it('should join several chunks parts when chunk does not ends with EOL', done => {
        StreamTest.v2
            .fromChunks(['line 1\nli', 'ne 2\n', 'line 3\n'])
            .pipe(new LineStream())
            .pipe(expectLines(done, 'line 1', 'line 2', 'line 3'));
    });

    it('should create a line for last chunk when no EOL', done => {
        StreamTest.v2
            .fromChunks(['line 1\nline 2'])
            .pipe(new LineStream())
            .pipe(expectLines(done, 'line 1', 'line 2'));
    });

    it('should create a line for last chunk when no EOL (with previous tail)', done => {
        StreamTest.v2
            .fromChunks(['line 1\nli', 'ne 2'])
            .pipe(new LineStream())
            .pipe(expectLines(done, 'line 1', 'line 2'));
    });

    it('should handle \\n, \\r and \\r\\n indifferently', done => {
        StreamTest.v2
            .fromChunks(['line 1\nline 2\rline 3\r\nline 4'])
            .pipe(new LineStream())
            .pipe(expectLines(done, 'line 1', 'line 2', 'line 3', 'line 4'));
    });
});
