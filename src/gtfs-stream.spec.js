const GtfsStream = require('./gtfs-stream');
const StreamTest = require('streamtest');

describe('GtfsStream', () => {
    // helpers

    const expectObjects = (done, ...expectedObjects) => {
        return StreamTest.v2.toObjects((err, actualObjects) => {
            if (err) {
                done(err);
            }

            expect(actualObjects).toEqual(expectedObjects);
            done();
        });
    };

    // tests

    it('transforms each line (except first) with comma separated values into objects', done => {
        StreamTest.v2
            .fromChunks([
                'agency_id,agency_name',
                'STIB-MIVB,STIB',
                'B,TEC Brabant wallon',
            ])
            .pipe(new GtfsStream())
            .pipe(
                expectObjects(
                    done,
                    {
                        agency_id: 'STIB-MIVB',
                        agency_name: 'STIB',
                    },
                    {
                        agency_id: 'B',
                        agency_name: 'TEC Brabant wallon',
                    },
                ),
            );
    });

    it('handles double quoted values', done => {
        StreamTest.v2
            .fromChunks(['"first","last"', '"sergio","mazzoleni"'])
            .pipe(new GtfsStream())
            .pipe(
                expectObjects(done, {
                    first: 'sergio',
                    last: 'mazzoleni',
                }),
            );
    });

    describe('internal methods', () => {
        describe('splitLine', () => {
            it('strips out double quotes when present at start AND end of value', () => {
                expect(GtfsStream.splitLine('"hello","world"')).toEqual([
                    'hello',
                    'world',
                ]);
            });

            it('unescapes two double quotes (start)', () => {
                expect(
                    GtfsStream.splitLine(
                        '"""value"" with quotes","""value"" with quotes"',
                    ),
                ).toEqual(['"value" with quotes', '"value" with quotes']);
            });

            it('unescapes two double quotes (inner)', () => {
                expect(
                    GtfsStream.splitLine(
                        '"first ""value"" with quotes","second ""value"" with quotes"',
                    ),
                ).toEqual([
                    'first "value" with quotes',
                    'second "value" with quotes',
                ]);
            });

            it('unescapes two double quotes (end)', () => {
                expect(
                    GtfsStream.splitLine(
                        '"first ""value""","second ""value"""',
                    ),
                ).toEqual(['first "value"', 'second "value"']);
            });

            it('takes comma inside quotes as part of the value (start)', () => {
                expect(GtfsStream.splitLine('",B",",D"')).toEqual([',B', ',D']);
            });

            it('takes comma inside quotes as part of the value (inner)', () => {
                expect(GtfsStream.splitLine('"A,B","C,D"')).toEqual([
                    'A,B',
                    'C,D',
                ]);
            });

            it('takes comma inside quotes as part of the value (end)', () => {
                expect(GtfsStream.splitLine('"A,","C,"')).toEqual(['A,', 'C,']);
            });

            it('throws when double quote inside value not wrapped by double quote', () => {
                expect(() =>
                    GtfsStream.splitLine('I am "Sergio"'),
                ).toThrowError('unwrapped double quote');
            });

            it('throws when double quote inside value is not precedeed by a double quote', () => {
                expect(() =>
                    GtfsStream.splitLine('"I am "Sergio"'),
                ).toThrowError('unescaped double quote');
            });

            it('handles empty value (start)', () => {
                expect(GtfsStream.splitLine(',A')).toEqual(['', 'A']);
            });

            it('handles empty value (inner)', () => {
                expect(GtfsStream.splitLine('A,,B')).toEqual(['A', '', 'B']);
            });

            it('handles empty value (end)', () => {
                expect(GtfsStream.splitLine('A,')).toEqual(['A', '']);
            });
        });
    });
});
